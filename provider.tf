terraform {
  required_providers {
    outscale = {
      source  = "outscale/outscale"
      version = "1.0.0-rc.1"
    }
  }
}

provider "outscale" {
  region = var.region
}
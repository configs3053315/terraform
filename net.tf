////////// VPC //////////
resource "outscale_net" "vpc_frl" {
    ip_range = "10.10.0.0/16"
    tenancy  = "default"
    tags {
        key   = "Name"
        value = "vpc_frl"
    }

}

////////// Subnets //////////
module "vpc_frl_subnet_adm" {
  source = "./subnet"

  cidr      = "10.10.1.0/24"
  subregion = var.availability_zones["a"]
  vpc       = resource.outscale_net.vpc_frl.net_id
  name      = "vpc_frl_subnet_adm"
}
module "vpc_frl_subnet_app" {
  source = "./subnet"

  cidr      = "10.10.2.0/24"
  subregion = var.availability_zones["b"]
  vpc       = resource.outscale_net.vpc_frl.net_id
  name      = "vpc_frl_subnet_app"
}

////////// Internet Gateway //////////
resource "outscale_internet_service" "vpc_frl_internet_gtw" {
}

resource "outscale_internet_service_link" "internet_service_link" {
    net_id              = outscale_net.vpc_frl.net_id
    internet_service_id = outscale_internet_service.vpc_frl_internet_gtw.id
}

////////// NAT Gateway //////////
resource "outscale_public_ip" "vpc_frl_nat_gtw_public_ip" {
}

resource "outscale_nat_service" "vpc_frl_nat_gateway" {
    subnet_id    = module.vpc_frl_subnet_adm.subnet_id
    public_ip_id = outscale_public_ip.vpc_frl_nat_gtw_public_ip.public_ip_id
}

////////// Route tables //////////
# Route Table 01
resource "outscale_route_table" "vpc_frl_route_table_01" {
    net_id = outscale_net.vpc_frl.net_id
}
# Route Table 01 LINK
resource "outscale_route_table_link" "vpc_frl_route_table_subnet_01" {
    subnet_id      = module.vpc_frl_subnet_adm.subnet_id
    route_table_id = outscale_route_table.vpc_frl_route_table_01.route_table_id
}
# Route Table 01 ROUTES
resource "outscale_route" "vpc_frl_rtb_01_routes" {
    gateway_id           = outscale_internet_service.vpc_frl_internet_gtw.internet_service_id
    destination_ip_range = "0.0.0.0/0"
    route_table_id       = outscale_route_table.vpc_frl_route_table_01.route_table_id
}

# Route Table 02
resource "outscale_route_table" "vpc_frl_route_table_02" {
    net_id = outscale_net.vpc_frl.net_id
}
# Route Table 02 LINK
resource "outscale_route_table_link" "vpc_frl_route_table_subnet_02" {
    subnet_id      = module.vpc_frl_subnet_app.subnet_id
    route_table_id = outscale_route_table.vpc_frl_route_table_02.route_table_id
}
# Route Table 02 ROUTES
resource "outscale_route" "vpc_frl_rtb_02_routes" {
    nat_service_id           = outscale_nat_service.vpc_frl_nat_gateway.nat_service_id
    destination_ip_range = "0.0.0.0/0"
    route_table_id       = outscale_route_table.vpc_frl_route_table_02.route_table_id
}
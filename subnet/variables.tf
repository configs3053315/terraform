variable "cidr" {
  description = "CIDR for the Subnet"
  type        = string
}

variable "subregion" {
  description = "Subregion in which to create the Subnet"
  type        = string
}

variable "vpc" {
  description = "ID of the VPC in which to create the Subnet"
  type        = string
}

variable "name" {
  description = "Name of the Subnet"
  type        = string
}
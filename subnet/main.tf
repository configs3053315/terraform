resource "outscale_subnet" "subnet" {
  net_id         = var.vpc
  ip_range       = var.cidr
  subregion_name = var.subregion

  tags {
    key   = "Name"
    value = var.name
  }
}
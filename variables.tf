/////////// Authentication ////////////
variable "access_key_id" {
  description = "AK"
  type        = string
  sensitive   = true
}
variable "secret_key_id" {
  description = "SK"
  type        = string
  sensitive   = true
}

/////////// Configuration ////////////
variable "availability_zones" {
  description = "Liste des zones de disponibilité"
  type        = map(any)
  default = {
    "a" = "eu-west-2a",
    "b" = "eu-west-2b",
  }
}
variable "keypair_name" {
  description = "Keypair par défault"
  type        = string
}
variable "region" {
  description = "La région Outscale où déployer les ressources"
  type        = string
  default     = "eu-west-2"
}

/////////// Instance ////////////
variable "vm_type" {
  description = "Type d'instance par défault"
  type        = string
  default     = "tinav4.c1r1p2"
}

variable "image_id" {
  description = "OMI par défault; Ubuntu 22"
  type        = string
  default     = "ami-a3ca408c"
}
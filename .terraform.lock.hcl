# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/outscale/outscale" {
  version     = "1.0.0-rc.1"
  constraints = "1.0.0-rc.1"
  hashes = [
    "h1:G3GawXgJJuDWP+fCrZc6bG5rWWQw9HekkchVvAVDPeA=",
    "zh:03f7a7daa7ceb848322330d98417c827de5a531a10d608662d24bf9e37ab10ce",
    "zh:5b210afd5f709374b3bdcdd6986e9bd8cfaf6fcce225dcd2d5ca8993d250190e",
    "zh:746b77f1afa89a2c6f13bd0dfa7c99a9c81dd98669c178503c3ccc865c791fb1",
    "zh:969463acd282e92195120dae57fa5aba2774dfdede6da929425d25dae9f067ca",
    "zh:b67d0309d34212d2fc40b5954fae186f5ddea0537295b117755cd737fdcb42b5",
    "zh:d40c55a8e298a130cc518798bd40ce8ee76a52f1557259a1e27a1c5262de6298",
  ]
}

resource "outscale_security_group" "ssh_security_group" {
  description         = "SSH security group"
  security_group_name = "ssh-security-group"
  net_id              = outscale_net.vpc_frl.net_id
}
resource "outscale_security_group" "ssh_internal_security_group" {
  description         = "SSH internal security group"
  security_group_name = "ssh-internal-security-group"
  net_id              = outscale_net.vpc_frl.net_id
}
resource "outscale_security_group" "http_security_group" {
  description         = "HTTP(s) security group"
  security_group_name = "http-security-group"
  net_id              = outscale_net.vpc_frl.net_id
}

/////////// Rules ///////////
resource "outscale_security_group_rule" "ssh_security_group_rule" {
    flow              = "Inbound"
    security_group_id = outscale_security_group.ssh_security_group.security_group_id
    from_port_range   = "22"
    to_port_range     = "22"
    ip_protocol       = "tcp"
    ip_range          = "0.0.0.0/0"
}
resource "outscale_security_group_rule" "ssh_internal_security_group_rule" {
    flow              = "Inbound"
    security_group_id = outscale_security_group.ssh_internal_security_group.security_group_id
    from_port_range   = "22"
    to_port_range     = "22"
    ip_protocol       = "tcp"
    ip_range          = "10.10.1.0/24"
}
resource "outscale_security_group_rule" "http_security_group_rule" {
    flow              = "Inbound"
    security_group_id = outscale_security_group.http_security_group.security_group_id
    from_port_range   = "80"
    to_port_range     = "80"
    ip_protocol       = "tcp"
    ip_range          = "0.0.0.0/0"
}
resource "outscale_security_group_rule" "https_security_group_rule" {
    flow              = "Inbound"
    security_group_id = outscale_security_group.http_security_group.security_group_id
    from_port_range   = "443"
    to_port_range     = "443"
    ip_protocol       = "tcp"
    ip_range          = "0.0.0.0/0"
}
resource "outscale_vm" "vpc_frl_vm_bastion" {
  image_id                 = var.image_id
  vm_type                  = var.vm_type
  keypair_name             = var.keypair_name
  security_group_ids       = [outscale_security_group.ssh_security_group.security_group_id]
  subnet_id                = module.vpc_frl_subnet_adm.subnet_id
  tags {
    key   = "name"
    value = "vpc_frl_bastion"
  }
}

resource "outscale_public_ip" "vpc_frl_public_ip_bastion" {
}

resource "outscale_public_ip_link" "vpc_frl_public_ip_bastion_link" {
    vm_id        = outscale_vm.vpc_frl_vm_bastion.vm_id
    public_ip    = outscale_public_ip.vpc_frl_public_ip_bastion.public_ip
    allow_relink = true
}

resource "outscale_vm" "vpc_frl_vm_test" {
  image_id                 = var.image_id
  vm_type                  = var.vm_type
  keypair_name             = var.keypair_name
  security_group_ids       = [outscale_security_group.ssh_internal_security_group.security_group_id]
  subnet_id                = module.vpc_frl_subnet_app.subnet_id
  tags {
    key   = "name"
    value = "vpc_frl_test_subnet_02"
  }
}
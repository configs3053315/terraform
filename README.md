Config:  
```bash
$ export OUTSCALE_ACCESSKEYID="myaccesskey"
$ export OUTSCALE_SECRETKEYID="mysecretkey"
```

```bash
$ touch terraform.tfvars
```
```yaml
terraform.tfvars

keypair_name = "xxxx"
```

Apply:  
```bash
$ terraform init
$ terraform validate
$ terraform apply
```

Create a VPC into Outscale Cloud with:  
- 2 subnets  
- 1 Internet gtw  
- 1 NAT gtw  
- Routes tables  
- VMs  